#!/bin/sh

certbot certonly --standalone -d kyle-dixon.com --email me@kyle-dixon.com -n --agree-tos --expand
/usr/sbin/nginx -g "daemon off;"
/usr/sbin/crond -f -d 8 &
