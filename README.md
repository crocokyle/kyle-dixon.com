# Creating an Enterprise Platform With a Raspberry Pi Kubernetes Cluster

## Summary

Over the past year, while the pandemic was going on, I've been studying and coding like a madman. I tried to dip into a little of every aspect a software engineer could deal with. My previous job lacked the scale of a large enterprise and I wanted to gain some experience in something more challenging. I powered through 9 certifications related to software and then started volunteering with a dev team that used DevOps. 

Once it was time for a brain-break, I started this project to host an online resume. I wanted the web app to be dynamic and not just hard-coded. I also didn't want to use a pre-built CMS. I also wanted it to be a demonstration of everything I had learned.

Eventually, my project evolved into these four subprojects:

### 1. Flask Web App
- A dashboard that lets me add or remove elements of my resume easily. This creates a JSON file with each resume element.
- Website that renders an online resume from a JSON file.
- A Bootstrap frontend for the website with a light/dark theme
- A python module that calls an API to render a PDF version of the resume from the JSON file.

### 2. Kubernetes Cluster

- A Kubernetes cluster running on Raspberry Pis that hosts the web app
- An Nginx load balancer that routes traffic to the appropriate pod
- An A+ SSL Certificate with auto-renewal from letencrypt
- Horizontal Autoscaling to cope with increased/decreased traffic

### 3. Infrastructure as Code

- A puppet deployment that automates the setup when adding new Raspberry Pis to the cluster
- Version control for the configuration management

### 4. CI/CD Pipelines
- A pipeline that builds, tests, checks code quality and security, and deploys the flask web app container to my Kubernetes cluster
- A pipeline that builds, tests, and deploys the Nginx container to the Kubernetes cluster.
- Automatic rollbacks to the previous version when builds fail

## Disclaimer

I want to start by saying, I'm not all-knowing. This guide will contain mistakes and inefficiencies. I highly encourage contributions to this guide by submitting a merge request. I chose to write this guide in markdown to make contributions and improvements easier. 

You will run into problems. Tech changes quickly, environments can vary, etc. This project was created to keep everything in VCS, following an IaC methodology to make it more sustainable. When you run into a problem, keep pressing forward. Don't give up when you run into an issue. Read the error messages, do some googling, and you'll be back on track quickly. If you find that something needs to be changed in this project, feel free to contribute or create an issue and I'll do my best to resolve it.

## The Hardware

### Raspberry Pi 4s
You can run your cluster with any number of Raspberry Pi 4s. You could probably get away with just a single Raspberry Pi 4 with at least 2 GB of RAM, but I recommend using at least two so that you can get a feel for controller managers vs. nodes. 

For my setup, I am using:

- `2x` Raspberry Pi 4 - 4 GB RAM
- `1x` Raspberry Pi 4 - 2 GB RAM
- `1x` Raspberry Pi 4 - 1 GB RAM

### MicroSD Cards
You'll need to use microSD cards with at least 8 GB of storage. 

I bought a 5-pack of these [Samsung EVO Plus 32 GB microSD cards](https://www.amazon.com/gp/product/B07NP96DX5/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1).

### USB-C Power Supply / Cable
You'll want to buy a power supply that can output a minimum of 2.5A at 5.1v. The recommended current for the Raspberry Pi 4 is a 3A power supply. [You can read about recommended power supplies here](https://www.raspberrypi.org/documentation/hardware/raspberrypi/power/README.md#:~:text=The%20power%20supply%20requirements%20differ,uses%20a%20USB%2DC%20connector.).

I went cheap and bought:
- `1x` [2-Pack Dual USB-C 2.4A 5.1v Power Supply](https://www.amazon.com/gp/product/B091XXFVHQ/ref=ppx_yo_dt_b_asin_title_o03_s02?ie=UTF8&th=1) 
- `2x` [2-pack USB-C to USB-C cables](https://www.amazon.com/gp/product/B08T5WPC53/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1).

### (Optional) Raspberry Pi Cluster Case

This isn't necessary, but it looks pretty and also keeps your cluster organized and cool. The case I bought was this [iUniker Raspberry Pi 4 Cluster Case](https://www.amazon.com/gp/product/B07CTG5N3V/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1). 